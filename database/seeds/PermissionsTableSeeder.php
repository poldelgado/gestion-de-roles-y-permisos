<?php

use Illuminate\Database\Seeder;
use Caffeinated\Shinobi\Models\Permission;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Permission::create([
        	'name' => 'Navegar Usuarios',
        	'slug' => 'users.index',
        	'description' => 'Lista y navega todos los usuarios del sistema',
        ]);

        Permission::create([
        	'name' => 'Ver detalle de Usuarios',
        	'slug' => 'users.show',
        	'description' => 'ver en detalle cada usuario del sistema',
        ]);

        Permission::create([
        	'name' => 'Edicion Usuarios',
        	'slug' => 'users.edit',
        	'description' => 'Editar cualquier dato de un usuario del sistema',
        ]);

        Permission::create([
        	'name' => 'Eliminar usuario',
        	'slug' => 'users.destroy',
        	'description' => 'Eliminar cualquier usuario del sistema',
        ]);


        //Roles

        Permission::create([
        	'name' => 'Navegar roles',
        	'slug' => 'roles.index',
        	'description' => 'Lista y navega todos los roles del sistema',
        ]);

        Permission::create([
        	'name' => 'Ver detalle de roles',
        	'slug' => 'roles.show',
        	'description' => 'ver en detalle cada rol del sistema',
        ]);

        Permission::create([
        	'name' => 'Creacion de roles',
        	'slug' => 'roles.create',
        	'description' => 'Crear un nuevo rol del sistema',
        ]);

        Permission::create([
        	'name' => 'Edicion roles',
        	'slug' => 'roles.edit',
        	'description' => 'Editar cualquier dato de un rol del sistema',
        ]);

        Permission::create([
        	'name' => 'Eliminar rol',
        	'slug' => 'roles.destroy',
        	'description' => 'Eliminar cualquier rol del sistema',
        ]);


        //Products

        Permission::create([
        	'name' => 'Navegar productos',
        	'slug' => 'products.index',
        	'description' => 'Lista y navega todos los productos del sistema',
        ]);

        Permission::create([
        	'name' => 'Ver detalle de productos',
        	'slug' => 'products.show',
        	'description' => 'ver en detalle cada producto del sistema',
        ]);

        Permission::create([
        	'name' => 'Creacion de productos',
        	'slug' => 'products.create',
        	'description' => 'Crear un nuevo producto del sistema',
        ]);

        Permission::create([
        	'name' => 'Edicion de producto',
        	'slug' => 'products.edit',
        	'description' => 'Editar cualquier dato de un producto del sistema',
        ]);

        Permission::create([
        	'name' => 'Eliminar producto',
        	'slug' => 'products.destroy',
        	'description' => 'Eliminar cualquier producto del sistema',
        ]);


    }
}
